import os
from tqdm import tqdm

abcnet_vedastr = './abcnet_vedastr'
yolor_vietocr =  './yolor_vietocr'
path_out = './submission'

count = 0
for name in sorted(os.listdir(yolor_vedastr)):

    f_out = open(os.path.join(path_out, name), 'w', encoding='utf8')
    with open(os.path.join(yolor_vietocr, name), 'r', encoding='utf8') as f:
        lines = f.readlines()
        count += len(lines)
        for ins in lines:
            bbox = ins.split(',',8)[:-1]
            label = ins.split(',',8)[-1]
            if '\t' in label:
                label = label.split('\t')[0]
            else:
                label = label[:-1]
            text = ','.join(bbox) + ',' + label + '\n'
            f_out.write(text)
    with open(os.path.join(abcnet_vietocr, name), 'r', encoding='utf8') as f:
        lines = f.readlines()
        count += len(lines)
        for ins in lines:
            bbox = ins.split(',',8)[:-1]
            label = ins.split(',',8)[-1]
            if '\t' in label:
                label = label.split('\t')[0]
            else:
                label = label[:-1]
            text = ','.join(bbox) + ',' + label + '\n'
            f_out.write(text)

