import argparse
import os
import platform
import shutil
import time
from pathlib import Path

import cv2
import torch
import torch.backends.cudnn as cudnn
from numpy import random
import uvicorn

from utils.google_utils import attempt_load
from utils.datasets import LoadStreams, LoadImages
from utils.general import (
    check_img_size, non_max_suppression, apply_classifier, scale_coords, xyxy2xywh, strip_optimizer)
from utils.plots import plot_one_box
from utils.torch_utils import select_device, load_classifier, time_synchronized

from models.models import *
from utils.datasets import *
from utils.general import *
from utils.image_utils import * 
from pydantic import BaseModel
from fastapi import FastAPI

device = str(0)
device = select_device(device)


def load_model():
    weights = 'weights/weight_yolor_final.pt'
    imgsz = 1280
    cfg = './cfg/custom_scenetext.cfg'
    half = device.type != 'cpu'  

    # Load model
    with torch.no_grad():
        model = Darknet(cfg, imgsz).cuda()
        model.load_state_dict(torch.load(weights, map_location=device)['model'])
        model.to(device).eval()
        if half:
            model.half()  # to FP16

    torch.cuda.empty_cache()
    return model
    

MODEL = load_model()

def preprocess(img0, half=True):
    img = letterbox(img0, new_shape=(1280,1280), auto_size=64)[0]
    img = img[:, :, ::-1].transpose(2, 0, 1)
    img = np.ascontiguousarray(img)
    img = torch.from_numpy(img).to(device)
    img = img.half() if half else img.float()  # uint8 to fp16/32
    img /= 255.0  # 0 - 255 to 0.0 - 1.0
    return img

def detect(image_inputs):
    half = device.type != 'cpu'  

    # dataset = ''

    
    images_tensor = [preprocess(img0) for img0 in image_inputs]
    
    if len(images_tensor) == 1:
        img = images_tensor[0]
        img = img.unsqueeze(0)
    else:
        img = torch.stack(images_tensor, dim=0)

    # Run inference
    pred = MODEL(img, augment=False)[0]
    pred = non_max_suppression(pred, 0.5, 0.5, classes=False, agnostic=False)

    outputs = []
    for p in pred:
        flag = True if len(p) > 0 else False 
        outputs.append(flag)
        print('Length of result is {}'.format(len(pred[0])))

    return outputs

# -----------------------------------------
class DataInput(BaseModel):
    image_data: list 

app = FastAPI()

@app.post('/api/ocr')
def ocr_endpoint(input_map: DataInput):
    base64_image = input_map.image_data
    images = [np.array(base64_to_image(img)) for img in base64_image]
    response = detect(images)
    
    return dict(status_code = 200, msg = '', data = response)

if __name__ == '__main__':
    # im = cv2.imread('data/test/images/img1.jpg')
    # print(im)
    # detect([im]*4)

    uvicorn.run(app,
                port=5001,
                host='0.0.0.0',
                log_level="debug",
                use_colors=True,)
