python3 detect.py \
--device 0 \
--cfg ./cfg/custom_scenetext.cfg  \
--weight ./weights/weight_yolor_final.pt \
--iou-thres 0.01 \
--source /scene_text_pipeline/Detecter/yolor/data/test/images/img1.jpg \
--output ../../Results/yolor \
