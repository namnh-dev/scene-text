import io
import base64
import codecs
import numpy as np

from PIL import Image


def read_image(path: str) -> np.ndarray:
    image = Image.open(path).convert("RGB")

    # numpy array format (H, W, C=3), channel order RGB
    return np.asarray(image)


def image_to_base64(image0: np.ndarray) -> str:
    image = Image.fromarray(image0, "RGB")

    buffered = io.BytesIO()
    image.save(buffered, format="PNG")

    return base64.b64encode(buffered.getvalue()).decode()


def base64_to_bytearr(byte64_str: str) -> bytes:
    bytearr = codecs.decode(codecs.encode(byte64_str, encoding="ascii"), encoding="base64")
    return bytearr


def base64_to_image(byte64_str: str, grayscale: bool = False):
    bytearr = base64_to_bytearr(byte64_str)

    if grayscale:
        return Image.open(io.BytesIO(bytearr)).convert("L")

    return Image.open(io.BytesIO(bytearr)).convert("RGB")
