CUDA_VISIBLE_DEVICES="0,1" \
python3 train.py \
	--batch-size 4 \
	--img 640 640 \
	--data ../data/data.yaml \
	--cfg cfg/yolor_p6.cfg \
	--weights './yolor_p6.pt' \
	--device 0 \
	--name yolor_p6 \
	--hyp ../data/hyp.scratch.1280.yaml \
	--epochs 1000
