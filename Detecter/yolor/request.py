from PIL import Image 
from utils.image_utils import * 
import requests 
import numpy as np 
import random 

image = Image.open('data/test/images/img1.jpg')
new_size = (random.randint(1280,2560), random.randint(1280,2560))
image = image.resize(new_size)
image_2 = Image.open('food.jpg')


for i in range(10):
    base64_image =image_to_base64(np.array(image))
    base64_image_2 = image_to_base64(np.array(image_2))

    base64_images = [ base64_image]

    response = requests.post(url='http://192.168.20.156:5001/api/ocr',
    json={"image_data": base64_images
        })

    print(response.json())
