import numpy as np
from pathlib import Path
from shapely.geometry import *
import os
import shutil
import zipfile

def order_points(pts):
    if isinstance(pts, list):
        pts = np.asarray(pts, dtype='float32')
    rect = np.zeros((4, 2), dtype='float32')
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    return rect

def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))

def main():
    label_dir =  Path("vintext_bkai/orin/test/labels")
    output_dir = Path("evaluation_scripts/datasets/evaluation")
    output_name = "gt_vintext_bkai"
    (output_dir / "temp").mkdir(parents=True, exist_ok=True)
    
    for label_path in sorted(label_dir.iterdir()):
        output_path = output_dir / "temp" / ("00" + label_path.name)
        
        with label_path.open("r", encoding="utf-8") as f:
            with output_path.open("w", encoding="utf-8") as out:
                iline = 1
                for line in f:
                    line = line.strip().split(",", 8)
                    bbox = line[:-1]
                    gt = line[-1]
                    if "###" in gt:
                        continue

                    pts = [(bbox[i], bbox[i+1]) for i in range(0, len(bbox) - 1, 2)]
                    pts = order_points(pts).tolist()

                    try:
                        pgt = Polygon(pts)
                    except Exception as e:
                        print(e)
                        print('An invalid detection in {} line {} is removed ... '.format(label_path.name, iline))
                        continue

                    if not pgt.is_valid:
                        print('An invalid detection in {} line {} is removed ... '.format(label_path.name, iline))
                        continue

                    pRing = LinearRing(pts)
                    if pRing.is_ccw:
                        pts.reverse()
                    outstr = ''
                    for ipt in pts[:-1]:
                        outstr += (str(int(ipt[0]))+','+ str(int(ipt[1]))+',')
                    outstr += (str(int(pts[-1][0]))+','+ str(int(pts[-1][1])))
                    outstr = outstr+',####' + gt
                    out.write(outstr + '\n')

                    iline += 1

    last = os.getcwd()
    os.chdir(str(output_dir / "temp"))
    zipf = zipfile.ZipFile("../" + output_name + ".zip", 'w', zipfile.ZIP_DEFLATED)
    zipdir('./', zipf)
    zipf.close()
    os.chdir(last)
    shutil.rmtree(str(output_dir / "temp"))

if __name__ == "__main__":
    main()