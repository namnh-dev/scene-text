from pathlib import Path
from os import getcwd

def main():
    dest = Path("vintext_bkai/val.txt")
    img_dir = Path("vintext_bkai/val")

    base = Path(getcwd())

    with dest.open('w') as f:
        for im in sorted(img_dir.iterdir()):
            if "jpg" in im.suffix:
                f.write(str(base / im) + '\n')


if __name__ == "__main__":
    main()