import shutil
from pathlib import Path

def main():
    img_dir = Path("/mlcv/Databases/VinText/vietnamese_original/vietnamese/test_image")
    label_dir = Path("/mlcv/Databases/VinText/vietnamese_original/vietnamese/labels")

    dest_dir = Path("vintext_bkai/orin/train")
    (dest_dir / "images").mkdir(parents=True, exist_ok=True)
    (dest_dir / "labels").mkdir(parents=True, exist_ok=True)

    idx = 1201
    for i in range(1201, 1501):
        label_name = "0" * (5 - len(str(idx))) + str(idx) + ".txt"
        img_name = "0" * (5 - len(str(idx))) + str(idx) + ".jpg"

        src_label_name = f"gt_{i}.txt"
        src_img_name = f"im{'0' * (4 - len(str(i))) + str(i)}.jpg"
        src_label_path = label_dir / src_label_name
        src_img_path = img_dir / src_img_name

        shutil.copy(str(src_label_path), str(dest_dir / "labels" / label_name))
        shutil.copy(str(src_img_path), str(dest_dir / "images" / img_name))

        idx += 1


if __name__ == "__main__":
    main()