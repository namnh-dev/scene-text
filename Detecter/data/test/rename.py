from os import listdir
import os
from os.path import isfile, join
mypath = './images'
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
count = 1
for i in onlyfiles: 
    source_img = './images/' + i
    des_img = './images/img'  + str(count) + '.jpg'

    source_txt = './labels/' + i[:-3] + 'txt'
    des_txt = './labels/img'  + str(count) + '.txt'
    count += 1
    os.rename(source_img, des_img)
    os.rename(source_txt, des_txt)
